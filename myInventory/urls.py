"""myInventory URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from chihuahuaInventory import views;
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', views.welcome,name='home'),
    path('about/', views.about,name='about'),
    path('register', views.register, name='register'),
    path('login', views.login, name='login'),
    path('logout', views.logout, name='logout'),
    path('upload', views.upload, name='upload-part'),
    path('parts/<str:part_type_to_find>',views.types, name="parts"),
    path('part/<int:part_to_find>', views.find_part, name="part"),
    path('update/<int:part_to_find>', views.update_part, name="update-part")
]

urlpatterns += staticfiles_urlpatterns()
