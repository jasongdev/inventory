from django import forms
from .models import Part

class PartCreate(forms.ModelForm):
    class Meta:
        model = Part
        fields = '__all__'