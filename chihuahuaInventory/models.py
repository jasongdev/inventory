from django.db import models

# Create your models here.

class Part(models.Model):
    name = models.CharField(max_length=50)
    picture = models.ImageField()
    company = models.CharField(max_length=50)
    part_type = models.CharField(max_length=50)
    stock = models.IntegerField(default=0)
    description = models.TextField()

    def __str__(self):
        return self.name

