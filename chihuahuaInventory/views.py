from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.contrib.auth import logout as do_logout
from django.contrib.auth import authenticate
from django.contrib.auth.forms import AuthenticationForm, UserCreationForm
from django.contrib.auth import login as do_login
from .models import Part
from .forms import PartCreate
import operator

def about(request):
    return render(request, 'about.html')

def upload(request):
    upload = PartCreate()
    if request.method == 'POST':
        upload = PartCreate(request.POST,request.FILES)
        if upload.is_valid():
            upload.save()
            return redirect('home')
        else:
            return HttpResponse("""your form is wrong, reload <a href = "{{url:'home'}}">reload</a>""")
    else:
        return render(request, 'upload_form.html',{'upload_form':upload})


def welcome(request):
    # Si estamos identificados devolvemos la portada
    if request.user.is_authenticated:
        return render(request, "home.html")
    return redirect('/login')

def register(request):
    form = UserCreationForm()
    if request.method == "POST":
        form = UserCreationForm(data=request.POST)
        if form.is_valid():

            user = form.save()

            if user is not None:
                do_login(request, user)
                return redirect('/')

    return render(request, "register.html", {'form': form})

def login(request):
    form = AuthenticationForm()
    if request.method == "POST":
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']

            user = authenticate(username=username, password=password)

            if user is not None:
                do_login(request, user)
                request.session['user_id'] = username
                return redirect('/')

    return render(request, "login.html", {'form': form})

def logout(request):
    do_logout(request)
    return redirect('/')

def types(request, part_type_to_find):
    part_sel = Part.objects.filter(part_type=part_type_to_find)
    return render(request, 'list.html',{'parts':part_sel,'part_type':part_type_to_find})

def find_part(request, part_to_find):
    part_sel = Part.objects.get(id=part_to_find)
    return render(request, 'part.html',{'part':part_sel})

def update_part(request, part_to_find):
    try:
        part_sel = Part.objects.get(id=part_to_find)
    except Part.DoesNotExist:
        return redirect('/')
    part_form = PartCreate(request.POST or None, instance = part_sel)
    if part_form.is_valid():
        part_form.save()
        return redirect(('/'), {'alert_flag': True})
    return render(request,'upload_form.html',{'upload_form':part_form})
    
